# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/azure/azure-cloud-mining-script/xmrstak/backend/backendConnector.cpp" "/home/azure/azure-cloud-mining-script/CMakeFiles/xmr-stak-backend.dir/xmrstak/backend/backendConnector.cpp.o"
  "/home/azure/azure-cloud-mining-script/xmrstak/backend/cpu/crypto/cryptonight_common.cpp" "/home/azure/azure-cloud-mining-script/CMakeFiles/xmr-stak-backend.dir/xmrstak/backend/cpu/crypto/cryptonight_common.cpp.o"
  "/home/azure/azure-cloud-mining-script/xmrstak/backend/cpu/hwlocMemory.cpp" "/home/azure/azure-cloud-mining-script/CMakeFiles/xmr-stak-backend.dir/xmrstak/backend/cpu/hwlocMemory.cpp.o"
  "/home/azure/azure-cloud-mining-script/xmrstak/backend/cpu/jconf.cpp" "/home/azure/azure-cloud-mining-script/CMakeFiles/xmr-stak-backend.dir/xmrstak/backend/cpu/jconf.cpp.o"
  "/home/azure/azure-cloud-mining-script/xmrstak/backend/cpu/minethd.cpp" "/home/azure/azure-cloud-mining-script/CMakeFiles/xmr-stak-backend.dir/xmrstak/backend/cpu/minethd.cpp.o"
  "/home/azure/azure-cloud-mining-script/xmrstak/backend/globalStates.cpp" "/home/azure/azure-cloud-mining-script/CMakeFiles/xmr-stak-backend.dir/xmrstak/backend/globalStates.cpp.o"
  "/home/azure/azure-cloud-mining-script/xmrstak/http/httpd.cpp" "/home/azure/azure-cloud-mining-script/CMakeFiles/xmr-stak-backend.dir/xmrstak/http/httpd.cpp.o"
  "/home/azure/azure-cloud-mining-script/xmrstak/http/webdesign.cpp" "/home/azure/azure-cloud-mining-script/CMakeFiles/xmr-stak-backend.dir/xmrstak/http/webdesign.cpp.o"
  "/home/azure/azure-cloud-mining-script/xmrstak/jconf.cpp" "/home/azure/azure-cloud-mining-script/CMakeFiles/xmr-stak-backend.dir/xmrstak/jconf.cpp.o"
  "/home/azure/azure-cloud-mining-script/xmrstak/misc/console.cpp" "/home/azure/azure-cloud-mining-script/CMakeFiles/xmr-stak-backend.dir/xmrstak/misc/console.cpp.o"
  "/home/azure/azure-cloud-mining-script/xmrstak/misc/executor.cpp" "/home/azure/azure-cloud-mining-script/CMakeFiles/xmr-stak-backend.dir/xmrstak/misc/executor.cpp.o"
  "/home/azure/azure-cloud-mining-script/xmrstak/misc/telemetry.cpp" "/home/azure/azure-cloud-mining-script/CMakeFiles/xmr-stak-backend.dir/xmrstak/misc/telemetry.cpp.o"
  "/home/azure/azure-cloud-mining-script/xmrstak/misc/uac.cpp" "/home/azure/azure-cloud-mining-script/CMakeFiles/xmr-stak-backend.dir/xmrstak/misc/uac.cpp.o"
  "/home/azure/azure-cloud-mining-script/xmrstak/misc/utility.cpp" "/home/azure/azure-cloud-mining-script/CMakeFiles/xmr-stak-backend.dir/xmrstak/misc/utility.cpp.o"
  "/home/azure/azure-cloud-mining-script/xmrstak/net/jpsock.cpp" "/home/azure/azure-cloud-mining-script/CMakeFiles/xmr-stak-backend.dir/xmrstak/net/jpsock.cpp.o"
  "/home/azure/azure-cloud-mining-script/xmrstak/net/socket.cpp" "/home/azure/azure-cloud-mining-script/CMakeFiles/xmr-stak-backend.dir/xmrstak/net/socket.cpp.o"
  "/home/azure/azure-cloud-mining-script/xmrstak/version.cpp" "/home/azure/azure-cloud-mining-script/CMakeFiles/xmr-stak-backend.dir/xmrstak/version.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BACKEND_TYPE=cpu"
  "CONF_NO_CUDA"
  "CONF_NO_OPENCL"
  "GIT_BRANCH=master"
  "GIT_COMMIT_HASH=05daedc"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "."
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/azure/azure-cloud-mining-script/CMakeFiles/xmr-stak-c.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
